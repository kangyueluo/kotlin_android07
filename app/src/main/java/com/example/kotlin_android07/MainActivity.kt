package com.example.kotlin_android07

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent

class MainActivity : AppCompatActivity() {

    val TAG : String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        when(event?.actionMasked)
        {
            MotionEvent.ACTION_DOWN ->{
                Log.d(TAG,"action down")
            }

            MotionEvent.ACTION_POINTER_DOWN ->{
                Log.d(TAG,"pointer down")
            }

            MotionEvent.ACTION_MOVE ->{
                Log.d(TAG,"action move")
            }

            MotionEvent.ACTION_POINTER_UP ->{
                Log.d(TAG,"pointer up")
            }

            MotionEvent.ACTION_UP ->{
                Log.d(TAG,"action up")

            }

        }

        return super.onTouchEvent(event)
    }
}
